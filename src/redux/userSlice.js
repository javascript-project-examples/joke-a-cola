import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

// fetch user logic to go into state
export const getSampleUser = createAsyncThunk(
    'user/sampleUser',
    ()=>{
        return fetch('https://randomuser.me/api/')
        .then(response=>response.json())
        .then(sampleUser =>{return sampleUser})
      }  
    )

export const userSlice = createSlice( {
    name:"user",
    initialState:{
        results: [
          {
            gender: "female",
            name: {
              title: "Mademoiselle",
              first: "Claire",
              last: "Dubois"
            },
            location: {
              street: {
                number: 9878,
                name: "Grande Rue"
              },
              city: "Weesen",
              state: "Luzern",
              country: "Switzerland",
              postcode: 6359,
              coordinates: {
                latitude: "-59.7933",
                longitude: "16.0100"
              },
              timezone: {
                offset: "-7:00",
                description: "Mountain Time (US & Canada)"
              }
            },
            email: "claire.dubois@example.com",
            login: {
              uuid: "3352f2f7-6c75-4de6-8564-73f2c09ba3a6",
              username: "crazykoala316",
              password: "heng",
              salt: "VCuZ4yFx",
              md5: "2238bb9862eea9b9a1a6267d5ca79569",
              sha1: "594f4a19eedd44e0484574c214e97e7c4065e485",
              sha256: "dfb62f066bd81f566e667659f6bf861a6d6798dd36b1490cebb01e9d53c8f938"
            },
            dob: {
              date: "1999-08-28T01:19:16.752Z",
              age: 24
            },
            registered: {
              date: "2005-03-31T22:16:47.211Z",
              age: 18
            },
            phone: "078 616 42 55",
            cell: "076 926 71 90",
            id: {
              name: "AVS",
              value: "756.4830.4596.92"
            },
            picture: {
              large: "https://randomuser.me/api/portraits/women/74.jpg",
              medium: "https://randomuser.me/api/portraits/med/women/74.jpg",
              thumbnail: "https://randomuser.me/api/portraits/thumb/women/74.jpg"
            },
            nat: "CH"
          }
        ],
        info: {
          seed: "d86e4110e6014d55",
          results: 1,
          page: 1,
          version: "1.4"
        }
      },
    reducers:{
        setTestUser: (state)=>{
            state.userName = "test user"
            state.isLogged = true
        },
        setUser: (state,action)=>{
            state.userName = action.payload
            state.isLogged = true
        }
    },
    extraReducers:{
        [getSampleUser.fulfilled]:(state,action)=>{
            // this is where we update state with the fetched data
            return action.payload
        },
    }
})

export const {setTestUser,setUser} = userSlice.actions
export const userReducer = userSlice.reducer

