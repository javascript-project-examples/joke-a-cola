import logo from './logo.svg';
import './App.css';
import LoginForm from './components/LoginForm';
import JokeRater from './components/JokeRater';
import {BrowserRouter, Route,NavLink, Routes} from 'react-router-dom'
import LoginPage from './components/LoginPage';
import JokeRaterPage from './components/JokeRaterPage';
import JokeRecommenderPage from './components/JokeRecommenderPage';

// [x] - Guarding
// [x] - Form for Login
// [] - Joke Rating Logic
// [] - fix errors feedback

function App() {
  return (
    <div className="App">
    <BrowserRouter>
    <NavLink to="/">Login</NavLink>
    <NavLink to="/rater">Rate Jokes</NavLink>
    <NavLink to="/report">See Recommendations</NavLink>
    <Routes>
      <Route path='/' element={<LoginPage/>}></Route>
      <Route path='/rater' element={<JokeRaterPage/>}></Route>
      <Route path='/report' element={<JokeRecommenderPage/>}></Route>
    </Routes>
    </BrowserRouter>
    </div>
  );
}

export default App;
