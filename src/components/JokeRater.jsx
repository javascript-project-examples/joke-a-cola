import { useEffect, useState } from "react";
import JokeDisplayer from "./JokeDisplayer";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
let roflJokeRates = [];
let currentFav = "";

function JokeRater(props) {
  let [jokes, setJokes] = useState([]);
  let [currentJoke, setCurrentJoke] = useState({});
  let userData = useSelector((state) => state.userData);
  const navigate = useNavigate();
  // needs to be accescible to the next page

  useEffect(() => getJokes(), []);

  function getJokes() {
    fetch(
      "https://v2.jokeapi.dev/joke/Programming,Miscellaneous,Pun,Spooky,Christmas?blacklistFlags=nsfw,religious,political,racist,sexist,explicit&type=twopart&amount=10"
    )
      .then((response) => response.json())
      .then((jokesResult) => {
        setJokes(jokesResult.jokes);
        setCurrentJoke(jokesResult.jokes[0]);
      });
  }

  function sortByFrequency(array) {
    var frequency = {};

    array.forEach(function(value) { frequency[value] = 0; });

    var uniques = array.filter(function(value) {
        return ++frequency[value] == 1;
    });

    return uniques.sort(function(a, b) {
        return frequency[b] - frequency[a];
    });
}

  function rateAsRofl() {
    // rate as funny
    roflJokeRates.push(currentJoke.category);
    // find out the current prefered category
    roflJokeRates = sortByFrequency(roflJokeRates)
    currentFav = roflJokeRates[0]

    if (jokes.length >= 1) {
      setCurrentJoke(jokes.pop());
    }

    if (jokes.length == 0) {
      navigate("/report");
    }

    console.log(`It seems you like ${currentFav} jokes`)
  }

  function rateAsMeh() {
    // rate as lame
    if (jokes.length >= 1) {
      setCurrentJoke(jokes.pop());
    }

    if (jokes.length == 0) {
      navigate("/report");
    }
  }

  return (
    <div>
      {localStorage.getItem("currentUser") != null && (
        <h2>{userData.results[0].login.username} is logged in</h2>
      )}
      {jokes.length > 0 && (
        <JokeDisplayer jokeData={currentJoke}></JokeDisplayer>
      )}
      <button onClick={rateAsRofl}>🤣</button>
      <button onClick={rateAsMeh}>😐</button>
    </div>
  );
}

export default JokeRater;
