import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { getSampleUser, setTestUser, setUser } from "../redux/userSlice";

function LoginForm() {
  const {register, handleSubmit, formState:{errors} } = useForm();
  const navigate = useNavigate()
  const dispatch = useDispatch()

  function login(data) {
    // update lifted state for user
    // dispatch(setTestUser())
    //dispatch(setUser(data.username))
    dispatch(getSampleUser())
   localStorage.setItem("currentUser",data.username)
   navigate("/rater")

  }

  return (
    <div>
      <form onSubmit={handleSubmit(login)}>
        <input type="text" {...register("username",{required:true,minLength:3})}/>

        {errors.username && <p>Not valid</p>}

        <button type="submit">Login</button>
      </form>
    </div>
  );
}

export default LoginForm;
